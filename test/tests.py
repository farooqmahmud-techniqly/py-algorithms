import unittest
from sort import quicksort

class QuicksortTests(unittest.TestCase):
    def test_array_is_sorted(self):
        original = [1, 7, 11, 9, 2, 0, 3]
        expected = [0, 1, 2, 3, 7, 9, 11]
        actual = quicksort.quicksort([1, 7, 11, 9, 2, 0, 3])
        self.assertEqual(actual, expected)
