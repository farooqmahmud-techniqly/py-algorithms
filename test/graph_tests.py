import unittest
from graph.graph import Graph


class GraphTests(unittest.TestCase):
    __edges = [
        (0, 5),
        (4, 3),
        (0, 1),
        (9, 12),
        (6, 4),
        (5, 4),
        (0, 2),
        (11, 12),
        (9, 10),
        (0, 6),
        (7, 8),
        (9, 11),
        (5, 3)
    ]

    def setUp(self):
        self.__graph = Graph(self.__edges)

    def test_summary_returns_number_of_edges_and_vertices(self):
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 13)

    def test_add_edge_add_new_edge_both_vertices_exist(self):
        self.__graph.add_edge((1, 3))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_new_edge_one_vertex_exist(self):
        self.__graph.add_edge((100, 1))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 14)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_new_edge_no_vertices_exist(self):
        self.__graph.add_edge((100, 200))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 15)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_new_edge_already_exists_graph_not_changed(self):
        self.__graph.add_edge((0, 6))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 13)

    def test_add_edge_add_cycle(self):
        self.__graph.add_edge((0, 0))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_multiple_cycles_to_same_vertex(self):
        self.__graph.add_edge((0, 0))
        self.__graph.add_edge((0, 0))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_cycles_to_new_vertex(self):
        self.__graph.add_edge((100, 100))
        self.__graph.add_edge((100, 100))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 14)
        self.assertEqual(info.edges, 14)

    def test_add_edge_add_cycles_to_multiple_vertices(self):
        self.__graph.add_edge((0, 0))
        self.__graph.add_edge((6, 6))
        info = self.__graph.summary()
        self.assertEqual(info.vertices, 13)
        self.assertEqual(info.edges, 15)


