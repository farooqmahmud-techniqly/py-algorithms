from collections import namedtuple

GraphSummary = namedtuple("GraphSummary", "vertices edges")


class Graph:
    def __init__(self, edges):
        self.__edge_count = len(edges)

        self.__vertices = {}
        for edge in edges:
            self.__vertices[edge[0]] = None
            self.__vertices[edge[1]] = None

        self.__vertex_count = len(self.__vertices.keys())
        self.__graph = self.__make_graph(edges)

    def summary(self):
        return GraphSummary(edges=self.__edge_count, vertices=self.__vertex_count)

    def add_edge(self, edge):
        if not edge[0] in self.__graph:
            self.__graph[edge[0]] = []

        if not edge[1] in self.__graph[edge[0]]:
            self.__graph[edge[0]].append(edge[1])
            self.__edge_count += 1

        self.__vertices[edge[0]] = None
        self.__vertices[edge[1]] = None
        self.__vertex_count = len(self.__vertices.keys())

    @staticmethod
    def __make_graph(edges):
        graph = {}

        for edge in edges:
            if not edge[0] in graph:
                graph[edge[0]] = []

            graph[edge[0]].append(edge[1])

        return graph
